# Hack the Box tools

These are just some cheasy scripts I've written to automate some things with
Hack the Box. They aren't guaranteed to all work perfectly.

## init-machine.sh

This script takes a htb machine ID as an argument and will create a nice
directory structure for your analysis, add the host to `/etc/hosts` and
initiate some port scans, if requested. The port scans are placed in the new
machine directory.

You'll need to find your htb API key and place it in the file `.htb-key` so
that the script can interact with the htb API as your user. Also, you'll need
the `htb-cli` pip package installed on your system. It uses the `hackthebox.py`
script that is installed with that package.

## enum-machine.py

This script attempts to do some basic enumeration based on the services found
during initial scanning with `init-machine.sh`. It's not really filled out yet,
and doesn't work very well. I was just tinkering with the idea.
