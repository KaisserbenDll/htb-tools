#!/usr/bin/env python3
""" enum-machine.py - Basic Hack the Box Machine Enumeration

enum-machine will analyze the initial scans run by init-machine, and perform
various other scans based on open ports/services observed.
"""
from colorama import Fore, Back, Style
from bs4 import BeautifulSoup
import subprocess
import argparse
import sys
import os
import re

def prompt(message, valid = ['y', 'n']):
	""" Print a nice message, and ask for input """
	decision = None
	while decision not in valid:
		decision = input('[{0}?{1}] {2} ({3})?'.format(Fore.CYAN, Style.RESET_ALL,
							message, '/'.join(valid)))
	return decision

def info(message):
	""" Print a nice info message """
	print('[{0}+{1}] {2}'.format(Fore.BLUE, Style.RESET_ALL, message))

def warning(message):
	""" print a nice warning message """
	print('[{0}*{1}] {2}'.format(Fore.YELLOW, Style.RESET_ALL, message))

def error(message):
	""" print a nice error message and exit """
	sys.stdout.write('[{0}!{1}] {2}\n'.format(Fore.RED, Style.RESET_ALL, message))
	sys.exit(1)

def scan_gobuster(host, hostname, port):
	output_path = os.path.join(args.machine, 'scans', 'gobuster.txt')
	url = '{0}://{1}:{2}'.format(port.find_all('service')[0]['name'],
			hostname, port['portid'])
	os.system(' '.join([
		'gobuster',
		'-w', '/usr/share/dirbuster/directory-list-2.3-medium.txt',
		'-f', '-k',
		'-u', url]))

def scan_enum4linux(host, hostname, port):
	output_path = os.path.join(args.machine, 'scans', 'enum4linux.txt')
	os.system(' '.join([
		'enum4linux', '-a', hostname, '|',
		'tee', output_path]))

def scan_nikto(host, hostname, port):
	info('running nikto')

KNOWN_SERVICES = [ {
		'name': 'HTTP File/Directory Bruteforcing (gobuster)',
		'ports': (80, 443),
		'service': '([Aa]pache|[Ww]eb|httpd?)',
		'execute': scan_gobuster
	}, {
		'name': 'Microsoft SMB Server Enumeration (enum4linux)',
		'ports': (445,),
		'service': 'microsoft-ds',
		'execute': scan_enum4linux
	}, {
		'name': 'Basic HTTP Server Vulnerability Scan (nikto)',
		'ports': (80, 443),
		'service': '(http|https|httpd)',
		'execute': scan_nikto
	}
]

# Build argument parser and parse arguments
parser = argparse.ArgumentParser()
parser.add_argument('--all', '-a',
		help='run all enumeration options without prompting')
parser.add_argument('machine',
		help='the path to the machine directory which init-machine created')
args = parser.parse_args()

# Ensure the machine directory exists
if not os.path.isdir(args.machine):
	error('{0}: no such directory'.format(args.machine))

# Initialize soup
udp_soup = None
tcp_soup = None

# Check if we have an all-ports scan, and is that
all_ports_path = os.path.join(args.machine, 'scans', 'all-ports.xml')
basic_path = os.path.join(args.machine, 'scans', 'basic.xml')
if os.path.exists(all_ports_path):
	with open(all_ports_path) as f:
		tcp_soup = BeautifulSoup(f, 'xml')
elif os.path.exists(basic_path):
	with open(basic_path) as f:
		tcp_soup = BeautifulSoup(f, 'xml')

# Check if we have an all-ports scan of udp, and use that
all_ports_path = os.path.join(args.machine, 'scans', 'udp-all-ports.xml')
basic_path = os.path.join(args.machine, 'scans', 'basic-udp.xml')
if os.path.exists(all_ports_path):
	with open(all_ports_path) as f:
		udp_soup = BeautifulSoup(f, 'xml')
elif os.path.exists(basic_path):
	with open(basic_path) as f:
		udp_soup = BeautifulSoup(f, 'xml')

if udp_soup is None and tcp_soup is None:
	error('no scan results found')

services = { 'tcp': {}, 'udp': {} }

if tcp_soup is not None:
	# Grab the host information
	host = tcp_soup.find_all('host')[0]
	hostname = host.find_all('hostname')[0]['name']

	# Iterate over services
	for state in tcp_soup.find_all(state='open'):
		port = state.parent
		service_name = port.find_all('service')[0]['name']
		port_name = '{0}/{1} {2}'.format(port['protocol'],port['portid'],
				service_name)
		
		for service in KNOWN_SERVICES:
			# Assume no match
			matches = False

			# Check if port is in the port list
			if 'ports' in service and port['portid'] in service['ports']:
				matches = True
			# Check if the service matches the regex
			elif 'service' in service and \
					re.match(service['service'], port.find_all('service')[0]['name']):
				matches = True
			if matches and \
					prompt('Run {0} on {1}'.format(service['name'],port_name)) == 'y':
				service['execute'](host, hostname, port)

		services['tcp'][port['portid']] = port

if udp_soup is not None:
	# Grab the host information
	host = tcp_soup.find_all('host')[0]

	# Iterate over services
	for state in udp_soup.find_all(state='open'):
		port = state.parent
		services['udp'][port['portid']] = port

